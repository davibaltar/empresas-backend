/**
 * Enterprises.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    email_enterprise: {
      type: 'STRING',
      required: false,
    },
    facebook: {
      type: 'STRING',
      required: false
    },
    twitter: {
      type: 'STRING',
      required: false
    },
    linkedin: {
      type: 'STRING',
      required: false
    },
    phone: {
      type: 'STRING',
      required: false
    },
    own_enterprise: {
      type: 'BOOLEAN',
      required: false
    },
    enterprise_name: {
      type: 'STRING',
      required: false
    },
    photo: {
      type: 'STRING',
      required: false
    },
    description: {
      type: 'STRING',
      required: false
    },
    city: {
      type: 'STRING',
      required: false
    },
    country: {
      type: 'STRING',
      required: false
    },
    value: {
      type: 'INTEGER',
      required: false
    },
    share_price: {
      type: 'FLOAT',
      required: false
    },
    enterprise_type: {
      type: 'STRING',
      required: false
    },
    toJSON: function () {
      var obj = this.toObject()
      return obj;
    }
  },

};

