var jwt = require('jsonwebtoken')
var secret = sails.config.secret;


module.exports = function (req, res, next) {
    var token = req.headers['access-token'];
    const username = req.headers['uid'];
    const client = req.headers['client'];

    if (token && sails.config.username === username) {
        jwt.verify(token, secret, function (err, decodedToken) {
            jwt.verify(client, secret, function (err, decodedClient) {
                if (err) return res.status(401).send({ success: false, message: 'invalid' });
                if (decodedToken && decodedClient) {
                    next();
                } else {
                    res.setHeader('Status', '404 Not Found')
                    return res.status(404).send({ errors: ['Authorized users only.']});
                }
            })
        });
    } else {
        res.setHeader('Status', '404 Not Found')
        return res.status(404).send({ errors: ['Authorized users only.']});
    }
};