/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var auth = require('../services/auth');

module.exports = {
    register: function (req,res) {
        auth.register(req,res);
    },
    login: function (req, res) {
        auth.login(req, res);
    }
};

