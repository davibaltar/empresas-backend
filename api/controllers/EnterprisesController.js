/**
 * EnterprisesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var auth = require('../services/auth');

module.exports = {

    getEnterprises: function (req, res) {
        var obj = {}

        if (req.params['id'])                           // Params
            obj['id'] = req.param('id')
        else
            Object.keys(req.query).forEach((key) => {   // Query
                if (key == 'enterprise_types' || key == 'enterprise_types')
                    obj['enterprise_type'] = { contains: '"id":' + req.query[key] + ',' }
                else
                    Object.keys(Enterprises.attributes).forEach((keyResult) => {
                        if (keyResult.includes(key))
                            obj[keyResult] = { contains: `${req.query[key]}` }
                    })
            });

        Enterprises.find(obj).exec(function (err, result) {
            if (result) {
                result.map(e => {
                    try {
                        e.enterprise_type = JSON.parse(e.enterprise_type)
                    } catch (err) {
                        console.log('Erro ao realizar o JSON.parse em EnterprisesController')
                    }
                    delete e['createdAt']
                    delete e['updatedAt']
                })
            }

            if (req.params['id'] && (!result || result.length == 0))
                return res.status(404).json({
                    "status": "404",
                    "error": "Not Found"
                })
            else {
                res = auth.standardHeader(res)
                return res.status(200).json({ enterprises: result || [] })
            }
        });
    }
};

