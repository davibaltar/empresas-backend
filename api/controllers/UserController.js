/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    register: function (req, res) {
        var email = req.param('email');
        var password = req.param('password');

        User.findOne({ email: email }).exec(function (err, result) {
            if (err) {
                return res.serverError(err);
            } else if (result) {
                return res.badRequest('Email ja cadastrado!\n' + JSON.stringify(result));
            } else {
                User.create({ email: email, password: password }).exec(function (err, result) {
                    if (err) {
                        return res.serverError(err)
                    }
                    return res.send({
                        success: true
                    });
                })
            }
        });
    }
};

