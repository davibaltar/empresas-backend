var passport = require('passport')
var jwt = require('jsonwebtoken')
const expiry = 14 * 24 * 60 * 60    // 1209600 | 14 dias
var token = null
var client = null
var uid = null


module.exports = {
    standardHeader: function (res) {
        res.setHeader('expiry', Math.floor(new Date().getTime() / 1000) + expiry)
        res.setHeader('X-XSS-Protection', '1; mode=block')
        res.setHeader('Cache-Control', 'max-age=0, private, must-revalidate')
        res.setHeader('Vary', 'Origin')
        res.setHeader('token-type', 'Bearer')
        res.setHeader('access-token', token)
        res.setHeader('uid', uid)
        res.setHeader('client', client)
        res.setHeader('X-Content-Type-Options', 'nosniff')
        res.setHeader('X-Frame-Options', 'SAMEORIGIN')
        return res
    },
    login: function (req, res) {
        req.body.username = req.body.email
        sails.config.username = req.body.email
        passport.authenticate('local', function (err, user) {
            if (!user) {
                res.setHeader('Status', '401 Unauthorized')
                res.status(401).send({
                    success: false,
                    "errors": [
                        "Invalid login credentials. Please try again."
                    ]
                });
                return;
            } else {
                if (err) {
                    res.status(400).send({
                        success: false,
                        message: 'Unknown Error',
                        error: err
                    });
                } else {
                    token = jwt.sign({ a: user[0].id, b: user[0].email }, sails.config.secret, { expiresIn: expiry })
                    client = jwt.sign({ a: user[0].email, b: user[0].password }, sails.config.secret, { expiresIn: expiry })
                    uid = user[0].email

                    res.setHeader('Status', '200 OK')
                    res.setHeader('expiry', Math.floor(new Date().getTime() / 1000) + expiry)
                    res.setHeader('X-XSS-Protection', '1; mode=block')
                    res.setHeader('Cache-Control', 'max-age=0, private, must-revalidate')
                    res.setHeader('Vary', 'Origin')
                    res.setHeader('token-type', 'Bearer')
                    res.setHeader('access-token', token)
                    res.setHeader('uid', uid)
                    res.setHeader('client', client)
                    res.setHeader('X-Content-Type-Options', 'nosniff')
                    res.setHeader('X-Frame-Options', 'SAMEORIGIN')
                    //res.setHeader('Transfer-Encoding', 'chunked')

                    var enterprise = user[0].enterprise
                    delete user[0]['createdAt']
                    delete user[0]['updatedAt']
                    delete user[0]['enterprise']

                    req.session.cookie.token = token;
                    res.send({
                        investor: user[0],
                        enterprise: enterprise,
                        success: true,
                    });
                }
            }
        })(req, res);
    }
};

