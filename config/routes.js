/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */
var blueprints = require('./blueprints');
const prefix = blueprints.blueprints.prefix || ''

module.exports.routes = {

  [`POST ${prefix}/users/register`]: 'UserController.register',
  [`POST ${prefix}/users/auth/sign_in`]: 'AuthController.login',
  [`GET ${prefix}/enterprises`]: 'EnterprisesController.getEnterprises',
  [`GET ${prefix}/enterprises/:id`]: 'EnterprisesController.getEnterprises'

};
